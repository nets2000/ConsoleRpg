﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.StartScreenFunctionality
{
    public static class StartScreen
    {
        /// <summary>
        /// Writes down all available options that the user has at the start of the game
        /// Returns a new game state based on the input of the user
        /// </summary>
        public static Program.GameState CreateStartScreenInterface()
        {
            Console.WriteLine("Welcome to Console RPG!");
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("1) Create character");
            Console.WriteLine("2) Close program");
            
            switch (Console.ReadLine())
            {
                case "1":
                    return Program.GameState.CharacterCreation;
                case "2":
                    return Program.GameState.Exit;
                default:
                    return Program.GameState.OnProgramOpen;
            }
        }
    }
}
