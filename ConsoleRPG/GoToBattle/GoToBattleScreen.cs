﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality;
using ConsoleRPG.ShowStats;

namespace ConsoleRPG.GoToBattle
{
    public static class GoToBattleScreen
    {
        /// <summary>
        /// Levels the character up
        /// Returns the user back to what to do screen
        /// </summary>
        public static Program.GameState CreateGoToBattleScreen()
        {
            Character character = CharacterManager.instance.character;

            Console.WriteLine(character.name + " went bravely into to battle to defeat hideous monsters!!!");
            Console.WriteLine(character.name + " leveled up!!!");

            character.OnCharacterLevelUp();
            Console.WriteLine(character.name + " is now level " + character.level);
            ShowStatsScreen.ShowCharacterStats();
            return Program.GameState.SelectWhatToDo;
        }
    }
}
