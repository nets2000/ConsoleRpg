﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.WhatToDoFunctionality
{
    public static class WhatToDoScreen
    {
        /// <summary>
        /// Writes down all available action options for what the user can do in the program once a character is created
        /// Returns a new game state based on the input of the user
        /// </summary>
        public static Program.GameState CreateWhatToDoInterface()
        {
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("1) Equip a weapon for you're character");
            Console.WriteLine("2) Equip some armor for you're character");
            Console.WriteLine("3) Show stats of character");
            Console.WriteLine("4) Go into battle with you're character");
            Console.WriteLine("5) Exit program");

            switch(Console.ReadLine())
            {
                case "1":
                    return Program.GameState.EquipWeapon;
                case "2":
                    return Program.GameState.EquipArmor;
                case "3":
                    return Program.GameState.ShowStats;
                case "4":
                    return Program.GameState.GoIntoBattle;
                case "5":
                    return Program.GameState.Exit;
                default:
                    return Program.GameState.SelectWhatToDo;
            }
        }
    }
}
