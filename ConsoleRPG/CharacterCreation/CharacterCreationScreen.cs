﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.ShowStats;

namespace ConsoleRPG.CharacterCreation
{
    public static class CharacterCreationScreen
    {
        private static int minimumNameLength = 3;
        private static int maximumNameLength = 16;

        private static Character.CharacterClass characterClass = Character.CharacterClass.None;
        private static string characterName = "";

        /// <summary>
        /// Creates all singletons required in game
        /// Returns the select what to do game state after the player created a character
        /// </summary>
        public static Program.GameState CreateCharacterCreationScreen()
        {
            new CharacterManager();
            new ArmorManager();
            new WeaponManager();

            PerformCharacterCreationPart(SelectRole);
            PerformCharacterCreationPart(CreateName);
            PerformCharacterCreationPart(SetStats);

            return Program.GameState.SelectWhatToDo;
        }

        /// <summary>
        /// Performs a given function while the characterCreationProcess bool is false
        /// </summary>
        private static void PerformCharacterCreationPart(Func<bool> characterCreationFunction)
        {
            bool characterCreationProcess = false;
            while (!characterCreationProcess)
            {
                characterCreationProcess = characterCreationFunction.Invoke();
            }
        }

        /// <summary>
        /// Gives player options for what character class the created character is going to be
        /// Returns true if the player gave a valif input for a character class else returns false
        /// </summary>
        private static bool SelectRole()
        {
            Console.Clear();
            Console.WriteLine("Select the class for you're character:");
            int amountOfClasses = Enum.GetNames(typeof(Character.CharacterClass)).Length - 1;
            for (int i = 0; i < amountOfClasses; i++)
            {
                Character.CharacterClass characterClass = (Character.CharacterClass)i;
                int number = i + 1;
                Console.WriteLine(number + ") " + characterClass);
            }

            string input = Console.ReadLine();
            int.TryParse(input, out int selectedClass);
            if (selectedClass > 0 && selectedClass <= amountOfClasses)
            {
                characterClass = (Character.CharacterClass)selectedClass - 1;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gives the player the option the give the character a certain name
        /// Returns true if name is valid else returns false
        /// Invalid names are:
        /// Names that contain less characters then the minimum requirement
        /// Names that contain more characters then the maximum amount of characters
        /// Names that start with a blank space
        /// Names that end with a blank space
        /// </summary>
        private static bool CreateName()
        {
            Console.WriteLine("What is the name of you're character?");
            string input = Console.ReadLine();
            if (input.Length < minimumNameLength)
            {
                Console.WriteLine("Character name has a minimum lenght of " + minimumNameLength);
                return false;
            } else if (input.Length > maximumNameLength)
            {
                Console.WriteLine("Character name has a maximum lenght of " + maximumNameLength);
                return false;
            }
            else if (char.IsWhiteSpace(input[0]))
            {
                Console.WriteLine("Character name can't start with a whitespace");
                return false;
            }
            else if (char.IsWhiteSpace(input[input.Length - 1]))
            {
                Console.WriteLine("Character name can't end with a whitspace");
                return false;
            }

            characterName = input;
            return true;
        }


        /// <summary>
        /// Informs user that a new character is created.
        /// Creates a new character in the character manager based on the chosen class and name
        /// Shows new character stats
        /// </summary>
        private static bool SetStats()
        {
            Console.WriteLine("Character created!");
            CharacterManager.instance.CreateCharacter(characterClass, characterName);
            ShowStatsScreen.ShowCharacterStats();
            return true;
        }
    }
}
