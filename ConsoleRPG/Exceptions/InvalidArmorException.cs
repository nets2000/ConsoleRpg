﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Exceptions
{
    /// <summary>
    /// Overrides exception class to set a costum error message
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message) : base(message) { }
    }
}
