﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG
{
    public static class CalcUtils
    {
        /// <summary>
        /// Returns a random double that is starts at the minimum (inclusive) and ends at maximum (exclusive)
        /// </summary>
        public static double GetRandomDoubleNumber(double minimum, double maximum)
        {
            Random r = new Random();
            return r.NextDouble() * (maximum - minimum) + minimum;
        }

        /// <summary>
        /// Rounds the float decimals down to the amount of given digits
        /// </summary>
        public static float Round(float value, int digits)
        {
            float mult = MathF.Pow(10, digits);
            return MathF.Round(value * mult) / mult;
        }
    }
}
