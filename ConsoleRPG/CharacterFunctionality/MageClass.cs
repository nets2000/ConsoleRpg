﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;

namespace ConsoleRPG.CharacterFunctionality
{
    class MageClass : Character
    {
        public MageClass(string name) : base(name)
        {
            possibleArmorTypes.AddRange(new Armor.ArmorTypes[] { Armor.ArmorTypes.Cloth });
            possibleWeaponTypes.AddRange(new Weapon.WeaponTypes[] { Weapon.WeaponTypes.Wand, Weapon.WeaponTypes.Staff });
        }

        public override void SetCharacterBaseStats()
        {
            characterClass = CharacterClass.Mage;
            characterStats.strength.stat = 1;
            characterStats.intelligence.stat = 8;
            characterStats.dexterity.stat = 1;
        }

        public override void UpdateCharacterStats()
        {
            characterStats.strength.stat += 1;
            characterStats.intelligence.stat += 5;
            characterStats.dexterity.stat += 1;
        }

        public override float CalculateCharacterDamage()
        {
            float totalStat = GetStatWithItemCalculatedIn(CharacterStats.TypeOfStat.Intelligence);
            return 1 + totalStat / 100;
        }
    }
}
