﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;

namespace ConsoleRPG.CharacterFunctionality
{
    class RogueClass : Character
    {
        public RogueClass(string name) : base(name)
        {
            possibleArmorTypes.AddRange(new Armor.ArmorTypes[] { Armor.ArmorTypes.Mail, Armor.ArmorTypes.Leather });
            possibleWeaponTypes.AddRange(new Weapon.WeaponTypes[] { Weapon.WeaponTypes.Dagger, Weapon.WeaponTypes.Sword });
        }

        public override void SetCharacterBaseStats()
        {
            characterClass = CharacterClass.Rogue;
            characterStats.strength.stat = 2;
            characterStats.intelligence.stat = 1;
            characterStats.dexterity.stat = 6;
        }

        public override void UpdateCharacterStats()
        {
            characterStats.strength.stat += 1;
            characterStats.intelligence.stat += 1;
            characterStats.dexterity.stat += 4;
        }

        public override float CalculateCharacterDamage()
        {
            float totalStat = GetStatWithItemCalculatedIn(CharacterStats.TypeOfStat.Dexterity);
            return 1 + totalStat / 100;
        }
    }
}
