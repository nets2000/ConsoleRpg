﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality
{
    public class CharacterManager
    {
        public static CharacterManager instance { get; private set; } = null;

        public Character character { get; private set; } = null;

        /// <summary>
        /// Creates a singleton of the character manager to be available anywhere
        /// </summary>
        public CharacterManager()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        /// <summary>
        /// Sets the character to a certain class based on the given character class
        /// Character gets given name
        /// </summary>
        public void CreateCharacter(Character.CharacterClass characterClass, string name)
        {
            switch (characterClass)
            {
                case Character.CharacterClass.Mage:
                    character = new MageClass(name);
                    break;
                case Character.CharacterClass.Warrior:
                    character = new WarriorClass(name);
                    break;
                case Character.CharacterClass.Ranger:
                    character = new RangerClass(name);
                    break;
                case Character.CharacterClass.Rogue:
                    character = new RogueClass(name);
                    break;
                default:
                    break;
            }
        }
    }
}
