﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality.CharacterEquipment
{
    /// <summary>
    /// Base template for an item
    /// itemName = the name of the item
    /// requiredLevel = minimum level before user can use item
    /// equipmentType = Type of item
    /// </summary>
    public abstract class Item
    {
        public enum EquipmentTypes
        {
            Weapon,
            HeadArmor,
            BodyArmor,
            LegArmor
        }

        public string itemName { get; private set; } = "";

        public int requiredLevel { get; private set; } = 0;

        public EquipmentTypes equipmentType { get; protected set; } = EquipmentTypes.BodyArmor;

        public Item(string itemName, int requiredLevel)
        {
            this.itemName = itemName;
            this.requiredLevel = requiredLevel;
        }
    }
}
