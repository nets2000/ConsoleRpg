﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor
{
    public class ArmorManager
    {
        public static ArmorManager instance { get; private set; } = null;

        private List<Armor> allArmor = new List<Armor>();

        /// <summary>
        /// Creates a singleton instance of the armor manager that is available anywhere
        /// Creates all armor on instantiation
        /// </summary>
        public ArmorManager()
        {
            if (instance == null)
            {
                instance = this;
                CreateAllArmor();
            }
        }

        /// <summary>
        /// Creates all armor based on the amount of armor types
        /// </summary>
        private void CreateAllArmor()
        {
            int amountOfArmorTypes = Enum.GetNames(typeof(Armor.ArmorTypes)).Length;
            Random r = new Random();
            int amountOfEquipmentTypes = Enum.GetNames(typeof(Item.EquipmentTypes)).Length;

            for (int i = 0; i < amountOfArmorTypes; i++)
            {
                AddArmorBasedOnType((Armor.ArmorTypes)i, amountOfEquipmentTypes, r);
            }
        }

        /// <summary>
        /// Creates armor based on the based on the amount of equipment types (amountOfEquipmentTypes) for armor
        /// For each equipment type creates 3 tiers of armor
        /// New armor gets the given the armorType
        /// </summary>
        private void AddArmorBasedOnType(Armor.ArmorTypes armorType, int amountOfEquipmentTypes, Random r)
        {
            for (int i = 1; i < amountOfEquipmentTypes; i++)
            {
                int startLevel = 1;
                int addedLevel = r.Next(1, 6);

                for (int j = 0; j < 3; j++)
                {
                    AddArmorBasedOnBodyPart(armorType, (Item.EquipmentTypes)i, startLevel, j + 1, r);
                    startLevel += addedLevel;
                }
            }
        }

        /// <summary>
        /// Adds the armor the allArmor list
        /// Newly created armor have given armor type & equipment type & required level
        /// Armor level is the tier of the weapon
        /// </summary>
        private void AddArmorBasedOnBodyPart(Armor.ArmorTypes armorType, Item.EquipmentTypes equipmentType, int requiredLevel, int armorLevel,
            Random r)
        {
            CharacterStats newArmorStats = new CharacterStats();
            for (int i = 0; i < newArmorStats.allStats.Count; i++)
            {
                newArmorStats.allStats[i].stat = r.Next(requiredLevel, requiredLevel * 3);
            }

            Armor newArmor = new Armor(armorType + " " + equipmentType + " level " + armorLevel, requiredLevel, equipmentType, armorType)
            {
                armorStats = newArmorStats
            };

            allArmor.Add(newArmor);
        }

        /// <summary>
        /// Gets a list of all armor that that are of the given armor type that user can equip with their current character level
        /// </summary>
        public List<Armor> FindAllArmorBasedOnMultipleTypesAndWithCorrectLevel(List<Armor.ArmorTypes> armorTypes)
        {
            Character character = CharacterManager.instance.character;
            return allArmor.FindAll(armor => armorTypes.Contains(armor.armorType) && armor.requiredLevel <= character.level);
        }

        /// <summary>
        /// Gets a list of all armor
        /// </summary>
        public List<Armor> GetAllArmor()
        {
            return allArmor;
        }
    }
}
