﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor
{
    /// <summary>
    /// A base template for all armor in the game
    /// armorType = the type of armor
    /// armorStats = the character stats this armor adds to the character if the character has this armor equipped
    /// </summary>
    public class Armor : Item
    {
        public enum ArmorTypes
        {
            Cloth,
            Mail,
            Leather,
            Plate
        }

        public ArmorTypes armorType { get; private set; } = ArmorTypes.Cloth;

        public CharacterStats armorStats { get; set; } = new CharacterStats();

        public Armor(string itemName, int requiredLevel, EquipmentTypes equipmentType, ArmorTypes armorType) : base(itemName, requiredLevel)
        {
            this.equipmentType = equipmentType;
            this.armorType = armorType;
        }
    }
}
