﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons
{
    /// <summary>
    /// A template for any weapon in the game
    /// weaponType = the type of the weapon
    /// damage = amount of damage per attack
    /// attackSpeed = amount of attack per second
    /// </summary>
    public class Weapon : Item
    {
        public enum WeaponTypes
        {
            Axe,
            Bow,
            Sword,
            Dagger,
            Hammer,
            Staff,
            Wand
        }

        public WeaponTypes weaponType { get; private set; } = WeaponTypes.Axe;
        
        public float damage { get; private set; } = 0;
        public float attackSpeed { get; private set; } = 0;

        public Weapon(string itemName, int requiredLevel, WeaponTypes weaponType, float damage, float attackSpeed) : 
            base (itemName, requiredLevel)
        {
            equipmentType = EquipmentTypes.Weapon;
            this.weaponType = weaponType;
            this.damage = damage;
            this.attackSpeed = attackSpeed;
        }
    }
}
