﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons
{
    class WeaponManager
    {
        public static WeaponManager instance { get; private set; } = null;

        private List<Weapon> allWeapons = new List<Weapon>();

        /// <summary>
        /// Weapon manager is set be be a singleton so it is accesible anywhere
        /// On instatiation creates all weapons that are available
        /// </summary>
        public WeaponManager()
        {
            if (instance == null)
            {
                instance = this;
                CreateAllWeapons();
            }
        }

        /// <summary>
        /// Create all weapons based on the amount of weapon types
        /// </summary>
        private void CreateAllWeapons()
        {
            int amountOfWeaponTypes = Enum.GetNames(typeof(Weapon.WeaponTypes)).Length;
            Random r = new Random();

            for (int i = 0; i < amountOfWeaponTypes; i++)
            {
                AddWeaponBasedOnType((Weapon.WeaponTypes)i, r);
            }
        }

        /// <summary>
        /// Create all weapons with three tiers that the weapons
        /// Each created weapon has the given weapon type
        /// Randomly increments the weapon level for each extra tier of the weapon
        /// </summary>
        private void AddWeaponBasedOnType(Weapon.WeaponTypes weaponType, Random r)
        {
            int startLevel = 1;
            int addedLevel = r.Next(1, 6);
            for (int i = 0; i < 3; i++)
            {
                AddWeaponBasedOnLevel(weaponType, startLevel, i + 1);
                startLevel += addedLevel;
            }
        }

        /// <summary>
        /// Adds the weapon the allWeapons list
        /// Newly created weapons have given weapon type & required level
        /// Weapon level is the tier of the weapon
        /// </summary>
        private void AddWeaponBasedOnLevel(Weapon.WeaponTypes weaponType, int requiredLevel, int weaponLevel)
        {
            // Create random damage for the weapon based on the required level rounded down to 2 decimals
            float damage = CalcUtils.Round((float)CalcUtils.GetRandomDoubleNumber(requiredLevel, requiredLevel * 3), 2);
            
            // Create random attack speed for the weapon based on the required level rounded down to 2 decimals
            float attackSpeed = CalcUtils.Round((float)CalcUtils.GetRandomDoubleNumber(requiredLevel / 4, requiredLevel / 2f), 2);
            Weapon newWeapon = new Weapon(weaponType + " level " + weaponLevel, requiredLevel, weaponType, damage, attackSpeed);

            allWeapons.Add(newWeapon);
        }

        /// <summary>
        /// Gets a list of all weapons that that are of the given weapon type that user can equip with their current character level
        /// </summary>
        public List<Weapon> FindAllWeaponsBasedOnMultipleTypesAndWithCorrectLevel(List<Weapon.WeaponTypes> weaponTypes)
        {
            Character character = CharacterManager.instance.character;
            return allWeapons.FindAll(weapon => weaponTypes.Contains(weapon.weaponType) && weapon.requiredLevel <= character.level);
        }

        /// <summary>
        /// Gets a list of all weapons
        /// </summary>
        public List<Weapon> GetAllWeapons()
        {
            return allWeapons;
        }
    }
}
