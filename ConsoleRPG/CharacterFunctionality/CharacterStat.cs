﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality
{
    /// <summary>
    /// Template for a stat of a character
    /// stat = current value of the stat
    /// type = the type of the stat
    /// </summary>
    public class CharacterStat
    {
        public int stat { get; set; } = 0;
        public CharacterStats.TypeOfStat type { get; set; } = CharacterStats.TypeOfStat.Strength;
        
        public CharacterStat(int stat, CharacterStats.TypeOfStat type)
        {
            this.type = type;
            this.stat = stat;
        }
    }
}
