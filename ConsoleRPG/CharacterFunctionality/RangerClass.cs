﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;

namespace ConsoleRPG.CharacterFunctionality
{
    class RangerClass : Character
    {
        public RangerClass(string name) : base(name)
        {
            possibleArmorTypes.AddRange(new Armor.ArmorTypes[] { Armor.ArmorTypes.Mail, Armor.ArmorTypes.Leather });
            possibleWeaponTypes.AddRange(new Weapon.WeaponTypes[] { Weapon.WeaponTypes.Bow});
        }

        public override void SetCharacterBaseStats()
        {
            characterClass = CharacterClass.Ranger;
            characterStats.strength.stat = 1;
            characterStats.intelligence.stat = 1;
            characterStats.dexterity.stat = 7;
        }

        public override void UpdateCharacterStats()
        {
            characterStats.strength.stat += 1;
            characterStats.intelligence.stat += 1;
            characterStats.dexterity.stat += 5;
        }

        public override float CalculateCharacterDamage()
        {
            float totalStat = GetStatWithItemCalculatedIn(CharacterStats.TypeOfStat.Dexterity);
            return 1 + totalStat / 100;
        }
    }
}
