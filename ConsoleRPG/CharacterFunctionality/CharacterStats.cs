﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CharacterFunctionality
{
    /// <summary>
    /// Saves all stats for character
    /// Each stat is given a typing based on an enum
    /// All stats are added to the all stat list
    /// </summary>
    public class CharacterStats
    {
        public enum TypeOfStat
        {
            Dexterity,
            Strength,
            Intelligence
        }

        public CharacterStat dexterity { get; set; } = new CharacterStat(0, TypeOfStat.Dexterity);
        public CharacterStat strength { get; set; } = new CharacterStat(0, TypeOfStat.Strength);
        public CharacterStat intelligence { get; set; } = new CharacterStat(0, TypeOfStat.Intelligence);

        public List<CharacterStat> allStats { get; private set; } = new List<CharacterStat>();

        public CharacterStats()
        {
            allStats.AddRange(new CharacterStat[] { dexterity, strength, intelligence });
        }

        /// <summary>
        /// Returns the stat of a given type
        /// </summary>
        public int GetStatOfType(TypeOfStat type)
        {
            return allStats.Find(stat => stat.type == type).stat;
        }
    }
}
