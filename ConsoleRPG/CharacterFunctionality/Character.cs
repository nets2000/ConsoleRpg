﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;

namespace ConsoleRPG.CharacterFunctionality
{
    /// <summary>
    /// A base template for any character class
    /// name = name of character
    /// Extra class explanation in the warrior class
    /// </summary>
    public abstract class Character
    {
        public enum CharacterClass
        {
            Warrior,
            Ranger,
            Rogue,
            Mage,
            None
        }

        public CharacterClass characterClass { get; protected set; } = CharacterClass.None;

        public List<Item> equipment { get; private set; } = new List<Item>();

        public List<Weapon.WeaponTypes> possibleWeaponTypes { get; protected set; } = new List<Weapon.WeaponTypes>();
        public List<Armor.ArmorTypes> possibleArmorTypes { get; protected set; } = new List<Armor.ArmorTypes>();

        public string name { get; private set; } = "";

        public int level { get; protected set; } = 1;

        public CharacterStats characterStats { get; private set; } = new CharacterStats();

        public Character(string name)
        {
            this.name = name;
            SetCharacterBaseStats();
        }

        /// <summary>
        /// Sets base values for character stats
        /// </summary>
        public abstract void SetCharacterBaseStats();

        /// <summary>
        /// Adds level and updates character stats
        /// </summary>
        public void OnCharacterLevelUp()
        {
            level++;
            UpdateCharacterStats();
        }

        /// <summary>
        /// Updates character stats
        /// </summary>
        public abstract void UpdateCharacterStats();

        /// <summary>
        /// Returns damage done done from the character based on whether the character has a weapon, weapon stats and character stats
        /// </summary>
        public float GetCharacterDamage()
        {
            Weapon weapon = (Weapon)equipment.Find(weapon => weapon.equipmentType == Item.EquipmentTypes.Weapon);
            float baseDamage = CalculateCharacterDamage();
            if (weapon == null) { return baseDamage; }

            float damage = (weapon.damage * weapon.attackSpeed) * baseDamage;
            return CalcUtils.Round(damage, 2);
        }

        /// <summary>
        /// Returns damage done from character with only base stats
        /// </summary>
        public abstract float CalculateCharacterDamage();

        /// <summary>
        /// Returns a character stat value with all extra item stats calculated in
        /// </summary>
        public int GetStatWithItemCalculatedIn(CharacterStats.TypeOfStat type)
        {
            int completeStat = characterStats.GetStatOfType(type);
            for (int i = 0; i < equipment.Count; i++)
            {
                completeStat += GetArmorStatWithType(equipment[i], type);
            }

            return completeStat;
        }

        /// <summary>
        /// Returns an int list of all character stats with item stats calculated in
        /// Character stats are given back in order of the allStat list in the characterStats class
        /// </summary>
        public List<int> GetAllStatsWithItemsCalculatedIn()
        {
            List<int> completeStats = new List<int>();
            for (int i = 0; i < characterStats.allStats.Count; i++)
            {
                int completeStat = characterStats.allStats[i].stat;
                for (int j = 0; j < equipment.Count; j++)
                {
                    completeStat += GetArmorStatWithType(equipment[j], characterStats.allStats[i].type);
                }
                completeStats.Add(completeStat);
            }

            return completeStats;
        }

        /// <summary>
        /// Returns the stat of the given armor of the given type
        /// If not armor returns 0
        /// </summary>
        private int GetArmorStatWithType(Item item, CharacterStats.TypeOfStat type)
        {
            if (item.GetType() != typeof(Armor)) { return 0; }

            Armor armor = (Armor)item;
            return armor.armorStats.GetStatOfType(type);
        }
    }
}
