﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;

namespace ConsoleRPG.CharacterFunctionality
{
    class WarriorClass : Character
    {
        /// <summary>
        /// In each character class constructor possible armor and weapon types for that character class are set
        /// </summary>
        public WarriorClass(string name) : base(name)
        {
            possibleArmorTypes.AddRange(new Armor.ArmorTypes[] { Armor.ArmorTypes.Mail, Armor.ArmorTypes.Plate });
            possibleWeaponTypes.AddRange(new Weapon.WeaponTypes[] { Weapon.WeaponTypes.Axe, Weapon.WeaponTypes.Hammer, 
                Weapon.WeaponTypes.Sword });
        }

        /// <summary>
        /// The base stats and character class are set
        /// </summary>
        public override void SetCharacterBaseStats()
        {
            characterClass = CharacterClass.Warrior;
            characterStats.strength.stat = 5;
            characterStats.intelligence.stat = 1;
            characterStats.dexterity.stat = 2;
        }

        /// <summary>
        /// The base stats are updated with the added stats per level up
        /// </summary>
        public override void UpdateCharacterStats()
        {
            characterStats.strength.stat += 3;
            characterStats.intelligence.stat += 1;
            characterStats.dexterity.stat += 2;
        }

        /// <summary>
        /// Returns base character damage based on with which stats the character class deals damage
        /// </summary>
        public override float CalculateCharacterDamage()
        {
            float totalStat = GetStatWithItemCalculatedIn(CharacterStats.TypeOfStat.Strength);
            return 1 + totalStat / 100;
        }
    }
}
