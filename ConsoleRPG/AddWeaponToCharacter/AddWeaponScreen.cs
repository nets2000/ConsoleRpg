﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Weapons;
using ConsoleRPG.CharacterFunctionality;
using ConsoleRPG.Exceptions;

namespace ConsoleRPG.AddWeaponToCharacter
{
    public static class AddWeaponScreen
    {
        /// <summary>
        /// Fetches all weapons the player can buy
        /// While the player is buying items shows the item buy screen
        /// Returns the what to do screen
        /// </summary>
        public static Program.GameState CreateAddWeaponScreen()
        {
            List<Weapon> buyableWeapons =
                WeaponManager.instance.FindAllWeaponsBasedOnMultipleTypesAndWithCorrectLevel(CharacterManager.instance.character.possibleWeaponTypes);
            bool buyingWeapons = true;
            while (buyingWeapons)
            {
                buyingWeapons = DisplayWeaponScreen(buyableWeapons);
            }

            return Program.GameState.SelectWhatToDo;
        }

        /// <summary>
        /// Shows all possible weapons that the player can buy
        /// Creates an exit shop number
        /// Returns true if player selected an item or if the player failed to give an correct input
        /// Returns false if player has the exit number as input
        /// </summary>
        private static bool DisplayWeaponScreen(List<Weapon> availableWeapons)
        {
            Console.Clear();
            Console.WriteLine("Available weapons for character to buy:");

            for (int i = 0; i < availableWeapons.Count; i++)
            {
                int number = i + 1;
                Console.WriteLine(number + ") " + availableWeapons[i].itemName);
                Console.WriteLine("\tRequired level: " + availableWeapons[i].requiredLevel);
                Console.WriteLine("\tType: " + availableWeapons[i].weaponType);
                Console.WriteLine("\tDamage: " + availableWeapons[i].damage);
                Console.WriteLine("\tAttack speed: " + availableWeapons[i].attackSpeed);
                Console.WriteLine("------------------------------");
            }

            int exitNumber = availableWeapons.Count + 1;
            Console.WriteLine(exitNumber + ") Exit the weapon store");

            string input = Console.ReadLine();
            int.TryParse(input, out int selectedWeaponInput);
            if (selectedWeaponInput > 0 && selectedWeaponInput <= exitNumber)
            {
                // Returns player to what to do screen
                if (selectedWeaponInput == exitNumber) { return false; }

                Weapon selectedWeapon = availableWeapons[selectedWeaponInput - 1];
                Character character = CharacterManager.instance.character;

                try
                {
                    // Throws error when character is below required level
                    if (character.level < selectedWeapon.requiredLevel)
                    {
                        throw new InvalidWeaponException("Character is too low leveled for currently selected weapon");
                    }
                    // Throws error when character class can't equip selected weapon type
                    else if (!character.possibleWeaponTypes.Contains(selectedWeapon.weaponType))
                    {
                        throw new InvalidWeaponException("Weapon type is invalid with character class");
                    }
                    else
                    {
                        // Sets player weapon to selected weapon
                        Weapon currentWeapon = (Weapon)character.equipment.Find(weapon => weapon.equipmentType == selectedWeapon.equipmentType);
                        if (currentWeapon != null)
                        {
                            character.equipment.Remove(currentWeapon);
                        }
                        character.equipment.Add(selectedWeapon);
                        Console.WriteLine("You're character is now wearing the " + selectedWeapon.itemName);
                    }
                }
                catch (InvalidWeaponException error)
                {
                    Console.WriteLine(error.GetType() + ": " + error.Message);
                }
            }
            else
            {
                Console.WriteLine("Please select an option");
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();

            return true;
        }
    }
}
