﻿using System;
using ConsoleRPG.StartScreenFunctionality;
using ConsoleRPG.CharacterCreation;
using ConsoleRPG.ShowStats;
using ConsoleRPG.WhatToDoFunctionality;
using ConsoleRPG.AddArmorToCharacter;
using ConsoleRPG.AddWeaponToCharacter;
using ConsoleRPG.GoToBattle;

namespace ConsoleRPG
{
    public class Program
    {
        public enum GameState
        {
            OnProgramOpen,
            CharacterCreation,
            SelectWhatToDo,
            EquipWeapon,
            EquipArmor,
            ShowStats,
            GoIntoBattle,
            Exit
        }

        private static GameState currentGameState = GameState.OnProgramOpen;

        /// <summary>
        /// While being in the while loop the program wil perform the function given in the play state
        /// </summary>
        static void Main(string[] args)
        {
            bool isPlaying = true;
            while (isPlaying)
            {
                isPlaying = PlayState();
            }
        }

        /// <summary>
        /// Show the current screen based on current game state
        /// Returns true if the user stays in program
        /// Returns false if user wants to exit program
        /// </summary>
        private static bool PlayState()
        {
            Console.Clear();

            switch (currentGameState)
            {
                case GameState.OnProgramOpen:
                    CreateOnProgramOpenScreen();
                    return true;
                case GameState.CharacterCreation:
                    CreateCharacterCreationScreen();
                    return true;
                case GameState.SelectWhatToDo:
                    CreateWhatToDoScreen();
                    return true;
                case GameState.EquipWeapon:
                    CreateAddWeaponScreen();
                    return true;
                case GameState.EquipArmor:
                    CreateAddArmorScreen();
                    return true;
                case GameState.ShowStats:
                    CreateShowStatsScreen();
                    return true;
                case GameState.GoIntoBattle:
                    CreateGoToBattleScreen();
                    return true;
                case GameState.Exit:
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Create the screen that is visible when the user starts the program
        /// </summary>
        private static void CreateOnProgramOpenScreen()
        {
            currentGameState = StartScreen.CreateStartScreenInterface();
        }

        /// <summary>
        /// Create the screen that is visible when the user creates a character
        /// </summary>
        private static void CreateCharacterCreationScreen()
        {
            currentGameState = CharacterCreationScreen.CreateCharacterCreationScreen();
        }

        /// <summary>
        /// Create the screen that is visible when the user can select for what screen to go to
        /// </summary>
        private static void CreateWhatToDoScreen()
        {
            currentGameState = WhatToDoScreen.CreateWhatToDoInterface();
        }

        /// <summary>
        /// Create the screen that is visible when the user wants to add armor to character
        /// </summary>
        private static void CreateAddArmorScreen()
        {
            currentGameState = AddArmorScreen.CreateAddArmorScreen();
        }

        /// <summary>
        /// Create the screen that is visible when the user wants to add a weapon to the character
        /// </summary>
        private static void CreateAddWeaponScreen()
        {
            currentGameState = AddWeaponScreen.CreateAddWeaponScreen();
        }

        /// <summary>
        /// Create the screen that is visible when the user wants to look at theire characters stats
        /// </summary>
        private static void CreateShowStatsScreen()
        {
            currentGameState = ShowStatsScreen.CreateShowStatsInterface();
        }

        /// <summary>
        /// Create the screen that is visible when the user wants to level up their character
        /// </summary>
        private static void CreateGoToBattleScreen()
        {
            currentGameState = GoToBattleScreen.CreateGoToBattleScreen();
        }
    }
}
