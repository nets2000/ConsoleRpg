﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment.Armor;
using ConsoleRPG.CharacterFunctionality;
using ConsoleRPG.Exceptions;

namespace ConsoleRPG.AddArmorToCharacter
{
    public static class AddArmorScreen
    {
        /// <summary>
        /// Fetches all armor the player can buy
        /// While the player is buying items shows the item buy screen
        /// Returns the what to do screen
        /// </summary>
        public static Program.GameState CreateAddArmorScreen()
        {
            List<Armor> buyableArmor =
                ArmorManager.instance.FindAllArmorBasedOnMultipleTypesAndWithCorrectLevel(CharacterManager.instance.character.possibleArmorTypes);
            bool buyingArmor = true;
            while (buyingArmor)
            {
                buyingArmor = DisplayArmorScreen(buyableArmor);
            }

            return Program.GameState.SelectWhatToDo;
        }

        /// <summary>
        /// Shows all possible armor that the player can buy
        /// Creates an exit shop number
        /// Returns true if player selected an item or if the player failed to give an correct input
        /// Returns false if player has the exit number as input
        /// </summary>
        private static bool DisplayArmorScreen(List<Armor> availableArmor)
        {
            Console.Clear();
            Console.WriteLine("Available armors for character to buy:");

            for (int i = 0; i < availableArmor.Count; i++)
            {
                int number = i + 1;
                Console.WriteLine(number + ") " + availableArmor[i].itemName);
                Console.WriteLine("\tRequired level: " + availableArmor[i].requiredLevel);
                Console.WriteLine("\tType: " + availableArmor[i].armorType);
                CharacterStats armorStats = availableArmor[i].armorStats;
                for (int j = 0; j < armorStats.allStats.Count; j++)
                {
                    Console.WriteLine("\t" + armorStats.allStats[j].type + ": " + armorStats.allStats[j].stat);
                }
                Console.WriteLine("------------------------------");
            }

            int exitNumber = availableArmor.Count + 1;
            Console.WriteLine(exitNumber + ") Exit the armor store");

            string input = Console.ReadLine();
            int.TryParse(input, out int selectedArmorInput);
            if (selectedArmorInput > 0 && selectedArmorInput <= exitNumber)
            {
                // Returns player to what to do screen
                if (selectedArmorInput == exitNumber) { return false; }

                Armor selectedArmor = availableArmor[selectedArmorInput - 1];
                Character character = CharacterManager.instance.character;

                try
                {
                    // Throws error when character is below required level
                    if (character.level < selectedArmor.requiredLevel)
                    {
                        throw new InvalidArmorException("Character is too low leveled for currently selected armor");
                    }
                    // Throws error when character class can't equip selected armor type
                    else if (!character.possibleArmorTypes.Contains(selectedArmor.armorType))
                    {
                        throw new InvalidArmorException("Armor type is invalid with character class");
                    }
                    else
                    {
                        // Sets player armor to selected armor
                        Armor armorOfSameType = (Armor)character.equipment.Find(armor => armor.equipmentType == selectedArmor.equipmentType);
                        if (armorOfSameType != null)
                        {
                            character.equipment.Remove(armorOfSameType);
                        }
                        character.equipment.Add(selectedArmor);
                        Console.WriteLine("You're character is now wearing the " + selectedArmor.itemName);
                    }
                } catch (InvalidArmorException error)
                {
                    Console.WriteLine(error.GetType() + ": " + error.Message);
                }
            } else
            {
                Console.WriteLine("Please select an option");
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();

            return true;
        }
    }
}
