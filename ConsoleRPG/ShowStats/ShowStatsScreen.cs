﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CharacterFunctionality;
using ConsoleRPG.CharacterFunctionality.CharacterEquipment;

namespace ConsoleRPG.ShowStats
{
    public static class ShowStatsScreen
    {
        /// <summary>
        /// Writes down all the current stats of the character inlcuding name, character class, level, items, base stats and damage
        /// Returns the user back to what to do screen
        /// </summary>
        public static Program.GameState CreateShowStatsInterface()
        {
            ShowCharacterStats();
            return Program.GameState.SelectWhatToDo;
        }

        /// <summary>
        /// Writes down all the current stats of the character inlcuding name, character class, level, items, base stats and damage
        /// </summary>
        public static void ShowCharacterStats()
        {
            Character character = CharacterManager.instance.character;

            string stats = "CharacterStats: \nName: " + character.name +
                "\nClass: " + character.characterClass +
                "\nLevel: " + character.level;

            int allEquipmentTypes = Enum.GetNames(typeof(Item.EquipmentTypes)).Length; 
            for (int i = 0; i < allEquipmentTypes; i++)
            {
                string equipmentTitle = "None";
                Item equipment = character.equipment.Find(item => item.equipmentType == (Item.EquipmentTypes)i);
                if (equipment != null)
                {
                    equipmentTitle = equipment.itemName;
                }

                stats += "\n" + (Item.EquipmentTypes)i + ": " + equipmentTitle;
            }

            CharacterStats characterStats = character.characterStats;
            List<int> characterStatsWithItems = character.GetAllStatsWithItemsCalculatedIn();
            for (int i = 0; i < characterStats.allStats.Count; i++)
            {
                stats += "\n" + characterStats.allStats[i].type + ": " + characterStatsWithItems[i];
            }

            stats += "\nDamage: " + character.GetCharacterDamage();
            stats += "\nPress enter to continue";

            Console.WriteLine(stats);
            Console.ReadLine();
        }
    }
}
