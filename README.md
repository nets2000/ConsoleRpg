# ConsoleRpg
A console RPG where you can select a character class. When a class has been selected and the character has been given a name you can equip armor and weapons for you're character. You can also level up you're character to unlock more armor and weapons and check you're characters stats.

# Installation & start-up
Fork to project to you're own git repository or download the zip file.<br>
Then open the project in visual studio. In visual studio double click this file:<br>
![alt text](/ConsoleRPG/ReadMeImages/Start-up1.PNG?raw=true)<br>
Then to start the program click this button:<br>
![alt text](/ConsoleRPG/ReadMeImages/Start-up2.PNG?raw=true)<br>

# Usage
To navigate the menu's in the program fill one of the given numbers the program gives as options in as input. Any other input will not be accepted. When the program states:<br> "Press enter to continue"<br>
Press enter. The name of the character has a minimum length of 3 and maximum length of 16. The name cant start or end with a whitespace.

# Make program show all weapons/ armor to test exceptions
The current program only shows weapons/ armor the character can equip. To test exceptions all possible equipment can be shown and 2 lines of code need to be edited. The two files from the image below need to be opened: <br>
![alt text](/ConsoleRPG/ReadMeImages/BeAbleToCreateException1.PNG?raw=true)<br>
These folders are in the root folder of the project. The following code snippets needs to be edited:<br>
![alt text](/ConsoleRPG/ReadMeImages/BeAbleToCreateException2.PNG?raw=true)<br>
![alt text](/ConsoleRPG/ReadMeImages/BeAbleToCreateException4.PNG?raw=true)<br>
These lines need to be changed to:<br>
![alt text](/ConsoleRPG/ReadMeImages/BeAbleToCreateException3.PNG?raw=true)<br>
![alt text](/ConsoleRPG/ReadMeImages/BeAbleToCreateException5.PNG?raw=true)<br>
Now the program will show all items.
